import { TExtendedConfig } from "./forms/textended-config.mjs";

export class TExtended {
    static MODULE_ID = "textended";
    static MODULE_PATH = `modules/${this.MODULE_ID}/`;
    static SETTINGS = {
        groups: `groups`,
        templates: `templates`,
        sheets: `sheets`,
        configuration: `configuration`,
        plugins: `plugins`,
    };
    static ELEMENTS = {
        BLOCK: ['div', 'blockquote', 'section', 'p', 'article', 'header', 'footer'],
        INLINE: ['span', 'b', 'i', 'textarea', 'label']
    }


    static log(force, ...args) {
        const shouldLog = force || game.modules.get('_dev-mode')?.api?.getPackageDebugValue(this.MODULE_ID);
    
        if (shouldLog) {
            console.log(this.MODULE_ID, '|', ...args);
        }
    }

    static initialize() {
        this.log(true, "Initializing TinyMCE Extended CSS...");

        game.TExtended = {
            ID: this.MODULE_ID,
            SETTINGS: this.SETTINGS,
            disableAllStylesheets: this.disableAllStylesheets,
            restoreDefaults: this.restoreDefaults
        };

        game.settings.register(this.MODULE_ID, this.SETTINGS.groups, {
            config: false,
            scope: 'world',
            default: [{ id: 'nt7jyq2038h2dr40', name: 'TExtended Themes', isEnabled: true, extensions: [
                {id: 'u94u321db5b13jkt', groupID: 'nt7jyq2038h2dr40', block: "section", title: "Shadowed Box", wrapper: true, split: false, classes: 'textendedShadowBox', isEnabled: true},
                {id: '504vmrgtz2uz783d', groupID: 'nt7jyq2038h2dr40', block: 'header', title: 'Fancy Header', wrapper: true, split: false, classes: 'textendedFancyHeader', isEnabled: true},
                {id: 'ijbepfjte3l6y52p', groupID: 'nt7jyq2038h2dr40', block: 'section', title: 'Narration', wrapper: true, split: false, classes: 'textendedShadowBox textendedNarration', isEnabled: true},
                {id: 's15u0gsdmqk3k7x7', groupID: 'nt7jyq2038h2dr40', block: 'section', title: 'Note', wrapper: true, split: false, classes: 'textendedShadowBox textendedNote', isEnabled: true}
            ] }],
            type: Array
        });
        //                {id: '', groupID: '', block: '', title: '', wrapper: true, split: true, classes: '', isEnabled: true}

        game.settings.register(this.MODULE_ID, this.SETTINGS.templates, {
            config: false,
            scope: 'world',
            default: [{
                id: '34prlpkl51t1kcq5',
                title: 'Left Sidebar',
                description: 'A left sidebar',
                content: '<aside class="sidebar" style="float: left; width: 20%;"><h3>Title here</h3><p class="textended-selected">{$contents}</p></aside>',
                isEnabled: true,
            },
            {
                id: 'kvaqcalf3q8c0l3a',
                title: 'Right Sidebar',
                description: 'A right sidebar',
                content: '<aside class="sidebar" style="float: right; width: 20%;"><h3>Title here</h3><p class="textended-selected">{$contents}</p></aside>',
                isEnabled: true,
            },
            {
                id: 'z73q100dqlfccotq',
                title: 'Toggle',
                description: 'A toggleable section of text',
                content: '<section><details><summary>Title here</summary><p class="textended-selected">{$contents}</p></details></section>',
                isEnabled: true,
            }],
            type: Array
        });

        game.settings.register(this.MODULE_ID, this.SETTINGS.sheets, {
            config: false,
            scope: 'world',
            default: [{ id: 'ahh8k7qc8yssbcki', name: 'TinyMCE Extended Sample', path: 'modules/textended/css/tmce-sample.css', locked: true, isEnabled: true}],
            type: Array
        });

        game.settings.registerMenu(this.MODULE_ID, this.SETTINGS.configuration, {
            name: game.i18n.localize("TEXTENDED.CONFIG.CONFIGURATION.label"),
            label: game.i18n.localize("TEXTENDED.CONFIG.CONFIGURATION.label"),
            restricted: true,
            icon: 'fas fa-wrench',
            type: TExtendedConfig
        });

        game.settings.register(this.MODULE_ID, this.SETTINGS.plugins, {
            name: game.i18n.localize("TEXTENDED.CONFIG.PLUGINS.name"),
            hint: game.i18n.localize("TEXTENDED.CONFIG.PLUGINS.hint"),
            config: true,
            scope: 'client',
            default: true,
            type: Boolean
        });

        this.initCSSFiles();
        this.initCSSStyles();
        if (game.settings.get(game.TExtended.ID, game.TExtended.SETTINGS.plugins)) this.initPlugins();
        this.initTemplates();
    }

    static initCSSFiles() {
        let sheets = game.settings.get(this.MODULE_ID, this.SETTINGS.sheets);
        for (let sheet of sheets) {
            if (!sheet.isEnabled) { continue; }

            let fileRef = document.createElement("link");
            fileRef.setAttribute("rel", "stylesheet");
            fileRef.setAttribute("type", "text/css");
            fileRef.setAttribute("href", sheet.path);
            fileRef.setAttribute("media", "all");
    
            document.head.appendChild(fileRef);

            // Push the stylesheet to the TinyMCE config
            CONFIG.TinyMCE.content_css.push(sheet.path);
        }
    }

    static initCSSStyles() {
        let groups = game.settings.get(this.MODULE_ID, this.SETTINGS.groups).filter(g => !g.isDisabled);

        for (let group of groups) {
            if(!group.extensions) { continue; }

            let items = group.extensions.map((g) => {
                let e = {
                    title: g.title, wrapper: g.wrapper, split: g.split, classes: g.classes.split(' '), block: null, inline: null
                };

                if (this.ELEMENTS.BLOCK.includes(g.block)) { e.block = g.block; } else { e.inline = g.block; }

                return e;
            });

            CONFIG.TinyMCE.style_formats =
            [...CONFIG.TinyMCE.style_formats,
                { title: group.name, items: items }
            ]
        }
    }

    static initTemplates() {
        CONFIG.TinyMCE.plugins = [...CONFIG.TinyMCE.plugins.split(" "), "template"].join(" ");
        CONFIG.TinyMCE.toolbar = [...CONFIG.TinyMCE.toolbar.split(" "), "template"].join(" ");
        CONFIG.TinyMCE.template_selected_content_classes += " textended-selected";
        CONFIG.TinyMCE.templates = CONFIG.TinyMCE.templates ?? [];

        const templates = game.settings.get(this.MODULE_ID, this.SETTINGS.templates).filter(g => !g.isDisabled);

        for (let template of templates) {
            CONFIG.TinyMCE.templates =
                [
                    ...CONFIG.TinyMCE.templates,
                    {
                        title: template.title,
                        description: template.description,
                        content: template.content + "<p></p>"
                    }
                ]
        }
    }

    static async disableAllStylesheets() {
        console.log("textended | Disabling all stylesheets...");
        let sheets = game.settings.get(game.TExtended.ID, game.TExtended.SETTINGS.sheets);
        for (let sheet of sheets) {
            sheet.isEnabled = false;
        }

        await game.settings.set(game.TExtended.ID, game.TExtended.SETTINGS.sheets, sheets);
        location.reload();
    }

    static async restoreDefaults() {
        let groups = [{ id: 'nt7jyq2038h2dr40', name: 'TExtended Themes', isEnabled: true, extensions: [
            {id: 'u94u321db5b13jkt', groupID: 'nt7jyq2038h2dr40', block: "section", title: "Shadowed Box", wrapper: true, split: false, classes: 'textendedShadowBox', isEnabled: true},
            {id: '504vmrgtz2uz783d', groupID: 'nt7jyq2038h2dr40', block: 'header', title: 'Fancy Header', wrapper: true, split: false, classes: 'textendedFancyHeader', isEnabled: true},
            {id: 'ijbepfjte3l6y52p', groupID: 'nt7jyq2038h2dr40', block: 'section', title: 'Narration', wrapper: true, split: false, classes: 'textendedShadowBox textendedNarration', isEnabled: true},
            {id: 's15u0gsdmqk3k7x7', groupID: 'nt7jyq2038h2dr40', block: 'section', title: 'Note', wrapper: true, split: false, classes: 'textendedShadowBox textendedNote', isEnabled: true}
        ] }];

        const templates = [{
            id: '34prlpkl51t1kcq5',
            title: 'Left Sidebar',
            description: 'A left sidebar',
            content: '<aside class="sidebar" style="float: left; width: 20%;"><h3>Title here</h3><p class="textended-selected">{$contents}</p></aside>',
            isEnabled: true,
        },
        {
            id: 'kvaqcalf3q8c0l3a',
            title: 'Right Sidebar',
            description: 'A right sidebar',
            content: '<aside class="sidebar" style="float: right; width: 20%;"><h3>Title here</h3><p class="textended-selected">{$contents}</p></aside>',
            isEnabled: true,
        },
        {
            id: 'z73q100dqlfccotq',
            title: 'Toggle',
            description: 'A toggleable section of text',
            content: '<section><details><summary>Title here</summary><p class="textended-selected">{$contents}</p></details></section>',
            isEnabled: true,
        }];

        let sheets = [{ id: 'ahh8k7qc8yssbcki', name: 'TinyMCE Extended Sample', path: 'modules/textended/css/tmce-sample.css', locked: true, isEnabled: true}];

        await game.settings.set(game.TExtended.ID, game.TExtended.SETTINGS.sheets, sheets);
        await game.settings.set(game.TExtended.ID, game.TExtended.SETTINGS.groups, groups);
        await game.settings.set(game.TExtended.ID, game.TExtended.SETTINGS.templates, templates);
    }

    static initPlugins() {
        import("./fontawesomepicker/plugin.js");

        const plugins = ["searchreplace", "visualchars", "visualblocks", "preview", "-fontawesomepicker"];

        CONFIG.TinyMCE.plugins = [...CONFIG.TinyMCE.plugins.split(" "), ...plugins].join(" ");
        CONFIG.TinyMCE.toolbar = [...CONFIG.TinyMCE.toolbar.split(" "), ...plugins.map(p => p.replace(/^-/, ""))].join(" ");

        CONFIG.TinyMCE.visualchars_default_state = true;
        CONFIG.TinyMCE.visualblocks_default_state = true;

        CONFIG.TinyMCE.fontawesomeUrl = "/fonts/fontawesome/css/all.min.css";
        tinymce.PluginManager.urls.fontawesomepicker = tinymce.documentBaseURL + "modules/textended/module/fontawesomepicker";
    }
}