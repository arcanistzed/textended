import { TExtended } from "../textended.mjs";

export class TExtendedConfig extends FormApplication {
    constructor(object, options) {
        super(object, options);

        this.hasChanges = false;
        this.initData();
    }

    static get defaultOptions() {
        const options = super.defaultOptions;

        return foundry.utils.mergeObject(options, {
            id: "textended-config",
            template: "modules/textended/templates/textended-config.html",
            submitOnChange: true,
            closeOnSubmit: false,
            width: 750,
            height: "auto",
            tabs: [{ navSelector: ".tabs", contentSelector: "#textend-config-content", initial: "tinymce" }]
        });
    }

    initData() {
        this.sheets = game.settings.get(TExtended.MODULE_ID, TExtended.SETTINGS.sheets);
        this.groups = game.settings.get(TExtended.MODULE_ID, TExtended.SETTINGS.groups);
        this.templates = game.settings.get(TExtended.MODULE_ID, TExtended.SETTINGS.templates);
    }

    activateListeners(html) {
        super.activateListeners(html);

        html.on('click', "[data-action]", this._handleButtonClick.bind(this));
    }

    async getData(options) {
        return {
            sheets: this.sheets,
            groups: this.groups,
            templates: this.templates,
            blockElements: TExtended.ELEMENTS.BLOCK,
            inlineElements: TExtended.ELEMENTS.INLINE
        };
    }

    get title() {
        return game.i18n.localize("TEXTENDED.CONFIG.CONFIGURATION.label")
    }

    async _handleButtonClick(event) {
        const clickedElement = $(event.currentTarget);
        const action = clickedElement.data().action;
        const parentID = clickedElement.parents("[data-id]")?.data()?.id;
        let _this = this;

        TExtended.log(false, "Button Click", parentID, clickedElement)

        switch (action) {
            case "addExtension":
                let extension = {
                    id: foundry.utils.randomID(16),
                    title: 'Something New',
                    groupID: parentID,
                    block: 'div',
                    wrapper: true,
                    split: true,
                    classes: []
                }

                this.groups.find(g => g.id === parentID).extensions.push(extension);
                this.hasChanges = true;
                await game.settings.set(TExtended.MODULE_ID, TExtended.SETTINGS.groups, this.groups);
                this.render(true);
                break;
            case "addNew":
                const newPath = clickedElement.parents("div.flexrow").children("[name='teNewSheet']").val();
                let newName = clickedElement.parents("div.flexrow").children("[name='teNewName']").val();

                if (!newPath || !newPath.endsWith(".css")) {
                    ui.notifications.warn(game.i18n.localize("TEXTENDED.ERRORS.valid-css"));
                    return;
                }

                if (this.sheets.find(s => s.path.toLowerCase() === newPath.toLowerCase())) {
                    ui.notifications.warn(game.i18n.localize("TEXTENDED.ERRORS.already-registered"));
                    return;
                }
        
                newName = !newName ? newPath : newName;
                let newSheet = {
                    id: foundry.utils.randomID(16),
                    name: newName,
                    path: newPath,
                    isEnabled: true,
                    locked: false
                }
        
                this.sheets.push(newSheet);
                this.hasChanges = true;
                await game.settings.set(TExtended.MODULE_ID, TExtended.SETTINGS.sheets, this.sheets);
                this.render(true);
                
                break;
            case "addNewGroup":
                const newGroupName = clickedElement.parents("div.flexrow").children("[name='teNewGroupName']").val();
                if (!newGroupName) {
                    ui.notifications.warn(game.i18n.localize("TEXTENDED.ERRORS.group-name-required"));
                    return;
                }

                let newGroup = {
                    id: foundry.utils.randomID(16),
                    name: newGroupName,
                    isEnabled: true,
                    extensions: []
                }

                this.groups.push(newGroup);
                await game.settings.set(TExtended.MODULE_ID, TExtended.SETTINGS.groups, this.groups);
                this.render(true);

                break;
            case "addNewTemplate":
                const newTemplateTitle = clickedElement.parents("div.flexrow").children("[name='teNewTemplateTitle']").val();
                const newTemplateDescription = clickedElement.parents("div.flexrow").children("[name='teNewTemplateDescription']").val();
                if (!newTemplateTitle) {
                    ui.notifications.warn(game.i18n.localize("TEXTENDED.ERRORS.template-title-required"));
                    return;
                }

                let newTemplate = {
                    id: foundry.utils.randomID(16),
                    title: newTemplateTitle,
                    description: newTemplateDescription,
                    content: `<p>A template</p>`,
                    isEnabled: true
                }

                this.templates.push(newTemplate);
                await game.settings.set(TExtended.MODULE_ID, TExtended.SETTINGS.templates, this.templates);
                this.render(true);

                break;
            case "deleteExtension":
                let groupID = clickedElement.parents("[data-group-id]")?.data()?.groupId;
                let extensionName = this.groups.find(g => g.id === groupID).extensions.find(e => e.id === parentID).title;

                new Dialog({
                    title: game.i18n.localize("TEXTENDED.confirm-delete"),
                    content: `${game.i18n.localize("TEXTENDED.confirm-delete-extension")} <b>${extensionName}</b>?`,
                    buttons: {
                        yes: {
                            icon: "<i class='fas fa-check'></i>",
                            label: game.i18n.localize("TEXTENDED.yes"),
                            callback: (html) => {
                                _this.groups.find(g => g.id === groupID).extensions = _this.groups.find(g => g.id === groupID).extensions.filter(e => e.id !== parentID);
                                _this.hasChanges = true;
                                (async () => { return await game.settings.set(TExtended.MODULE_ID, TExtended.SETTINGS.groups, this.groups); })().then(r => {
                                    this.render(true);
                                });
                            }
                        }, 
                        no: {
                            icon: "<i class='fas fa-times'></i>",
                            label: game.i18n.localize("TEXTENDED.cancel")
                        }
                    },
                    default: "no"
                }).render(true);

                break;
            case "deleteGroup":
                let groupName = this.groups.find(g => g.id === parentID).name;
                new Dialog({
                    title: `${groupName}`,
                    content: game.i18n.localize("TEXTENDED.confirm-delete-group"),
                    buttons: {
                        yes: {
                            icon: "<i class='fas fa-check'></i>",
                            label: game.i18n.localize("TEXTENDED.yes"),
                            callback: (html) => {
                                _this.groups = _this.groups.filter(g => g.id !== parentID);
                                _this.hasChanges = true;
                                (async () => { return await game.settings.set(TExtended.MODULE_ID, TExtended.SETTINGS.groups, this.groups); })().then(r => {
                                    this.render(true);
                                });
                            }
                        }, 
                        no: {
                            icon: "<i class='fas fa-times'></i>",
                            label: game.i18n.localize("TEXTENDED.cancel")
                        }
                    },
                    default: "no"
                }).render(true);
                break;
            case "deleteTemplate":
                let templateTitle = this.templates.find(e => e.id === parentID).title;

                new Dialog({
                    title: `${templateTitle}`,
                    content: `${game.i18n.localize("TEXTENDED.confirm-delete-template")}`,
                    buttons: {
                        yes: {
                            icon: "<i class='fas fa-check'></i>",
                            label: game.i18n.localize("TEXTENDED.yes"),
                            callback: (html) => {
                                _this.templates = _this.templates.filter(g => g.id !== parentID);
                                _this.hasChanges = true;
                                (async () => { return await game.settings.set(TExtended.MODULE_ID, TExtended.SETTINGS.templates, this.templates); })().then(r => {
                                    this.render(true);
                                });
                            }
                        },
                        no: {
                            icon: "<i class='fas fa-times'></i>",
                            label: game.i18n.localize("TEXTENDED.cancel")
                        }
                    },
                    default: "no"
                }).render(true);

                break;
            case "deleteSheet":
                this.sheets = this.sheets.filter(s => s.id != parentID);
                this.hasChanges = true;
                await game.settings.set(TExtended.MODULE_ID, TExtended.SETTINGS.sheets, this.sheets);
                this.render(true);
                break;
            case "submit":
                if (this.hasChanges) {
                    //Updating these settings require reloading.
                    location.reload();
                }

                this.close();
            default:
                break;
        }
    }

    async _updateObject(event, formData) {
        //Check Sheet Changes
        for (let sheet of this.sheets) {
            let update = {
                name: formData[`tSheetName-${sheet.id}`],
                path: formData[`tSheetPath-${sheet.id}`],
                isEnabled: formData[`tSheetIsEnabled-${sheet.id}`]
            };

            if (!sheet.locked && sheet.name != update.name) {
                this.hasChanges = true;
                sheet.name = update.name;
            }

            if (sheet.isEnabled != update.isEnabled) {
                this.hasChanges = true;
                sheet.isEnabled = update.isEnabled;
            }
        }

        for (let group of this.groups) {
            let update = {
                name: formData[`tGroupName-${group.id}`],
                isEnabled: formData[`tGroupIsEnabled-${group.id}`]
            };

            if (group.name !== update.name) {
                group.name = update.name;
                this.hasChanges = true;
            }

            if (group.isEnabled !== update.isEnabled) {
                group.isEnabled = update.isEnabled;
                this.hasChanges = true;
            }

            //Extensions
            for (let extension of group.extensions) {
                let eUpdate = {
                    title: formData[`tTitle-${extension.id}`],
                    block: formData[`tBlock-${extension.id}`],
                    wrapper: formData[`tWrapper-${extension.id}`],
                    split: formData[`tSplit-${extension.id}`],
                    classes: formData[`tClasses-${extension.id}`],
                    isEnabled: formData[`tIsEnabled-${extension.id}`]
                }

                if (extension.title !== eUpdate.title) {
                    extension.title = eUpdate.title;
                    this.hasChanges = true;
                }

                if (extension.block !== eUpdate.block) {
                    extension.block = eUpdate.block;
                    this.hasChanges = true;
                }

                if (extension.wrapper !== eUpdate.wrapper) {
                    extension.wrapper = eUpdate.wrapper;
                    this.hasChanges = true;
                }

                if (extension.split !== eUpdate.split) {
                    extension.split = eUpdate.split;
                    this.hasChanges = true;
                }

                if (extension.classes !== eUpdate.classes) {
                    extension.classes = eUpdate.classes;
                    this.hasChanges = true;
                }

                if (extension.isEnabled !== eUpdate.isEnabled) {
                    extension.isEnabled = eUpdate.isEnabled;
                    this.hasChanges = true;
                }
            }
        }

        for (let template of this.templates) {
            let update = {
                title: formData[`tTemplateTitle-${template.id}`],
                description: formData[`tTemplateDescription-${template.id}`],
                content: formData[`tTemplateContent-${template.id}`],
                isEnabled: formData[`tTemplateIsEnabled-${template.id}`]
            };

            if (template.title != update.title) {
                this.hasChanges = true;
                template.title = update.title;
            }

            if (template.description != update.description) {
                this.hasChanges = true;
                template.description = update.description;
            }

            if (template.content != update.content) {
                this.hasChanges = true;
                template.content = update.content;
            }

            if (template.isEnabled != update.isEnabled) {
                this.hasChanges = true;
                template.isEnabled = update.isEnabled;
            }
        }

        if (this.hasChanges) {
            await game.settings.set(TExtended.MODULE_ID, TExtended.SETTINGS.sheets, this.sheets);
            await game.settings.set(TExtended.MODULE_ID, TExtended.SETTINGS.groups, this.groups);
            await game.settings.set(TExtended.MODULE_ID, TExtended.SETTINGS.templates, this.templates);
        }
    }
}