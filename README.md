# TExtended - The Tiny MCE Extender

A helpful module for easily adding in options to the Tiny MCE editor - including the option to add in custom stylesheets.

Useful for adventure writers to be able to easily customize things like journal entries, or any area where the Tiny MCE editor is utilized.  This module is not required to be installed for the styles to work - all that is needed is the stylesheet the TinyMCE styles reference.

## Usage

### Registering Stylesheets
You do not need to register a stylesheet if it is already loaded some other way - ie, if another module, your system, or your world already loads the stylesheet then there is nothing for you to do here.

This section is only for adding in external stylesheets that are not already loaded and without wanting to edit your world.json.

### Extending the TinyMCE Editor

The TinyMCE Editor's predefined options are split into two categories - groups and extensions.

You must first define a group that will contain all the individual styles (also known as extensions) you wish to add.

Then, under each group, you can have as many extensions as you want.  Each extension has the following properties:
 - Title: What the extension will display as
 - Element: The name of the HTML element that the extension gets wrapped in.  Block elements force new lines, inline elements can be placed in the middle of a sentence.
 - Wrapper: This format is a container format for block elements - if the Element above is a block element, this should be enabled.
 - Split: Whether this extension should split the existing element it is being placed in.  
  For instance, disabled would be: ``<p>This is <span>not split</span> text.</p>`` but enabled would be ``<p>This is </p><span>now split</span><p> text.</p>``
 - Classes: A list of CSS classes that this extension will apply when use.  Separate each with a space, and do not use any dots.

## Troubleshooting
The TinyMCE Editor, while generally easy to use, is not a very "smart" tool and it is very easy for it to get confused and produce horrible code behind the scenes.  It may be necessary to manually edit the code to clean up the messes it makes - unfortunately there isn't really much that this module can do to help with that.  All this module does is provide a method for calling TinyMCE's API to add the requested groups and extensions.  If you cannot undo something done with the editor, you need to click the code < > button and manually adjust it.

Because this module allows you to register any stylesheet and it is very possible for a CSS stylesheet to create a situation in which you can no longer access settings in Foundry, there is a console command to force disable all registered stylesheets.  Open the developer console (typically via F12) and use the following command:
    game.TExtended.disableAllStylesheets();
The window will reload, and all stylesheets registered with TExtended will now be disabled.

If for some reason you wish to reset all the groups and extensions to default, use the following command:
    game.TExtended.restoreDefaults();
This will delete all existing groups and extensions and restore everything to default.